#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  echo "Script must be run as user: root"
  exit 255
fi

if ! command -v m365 &> /dev/null
then
  if ! command -v npm &> /dev/null
  then
    if ! command -v nvm &> /dev/null
    then
      curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
      export NVM_DIR="$HOME/.nvm"
      [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
      [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    fi
    nvm install 14 # install npm
  fi
  npm install -g @pnp/cli-microsoft365 # install m365 CLI
fi

if [ "$(m365 status)" = "Logged out" ]; then
  m365 login
fi

curl https://gitlab.com/romain.moventes/scripts-memo/-/raw/main/db_backup.sh -o /root/backup_db_and_upload

chmod 755 /root/backup_db_and_upload

echo -e "add this cronjob to the crontab (crontab -e): \n@daily /root/backup_db_and_upload -YOUR_SERVER_NAME >> /var/log/backup_db_and_upload 2>&1"
