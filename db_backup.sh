#!/bin/bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH="$PATH:/snap/bin"

docker="$(which docker)"
m365="$(which m365)"

date=`date +"%d-%m-%Y"`
filename="speakylink-$date.dump"

parent_path="/var/snap/docker/common/var-lib-docker/volumes/postgres-data/_data/"
container_path="/var/lib/postgresql/data/"

sharepoint_url="https://syd365.sharepoint.com/sites/Moventes"
sharepoint_folder_path="/Documents partages/speakylink/backup statistiques"

$docker exec -i pg-sl-stats pg_dump --create -Ft speakylink -U speakylink -f "$container_path$filename$1"
$m365 spo file add --webUrl "$sharepoint_url" --folder "$sharepoint_folder_path" --path "$parent_path$filename$1"
rm "$parent_path$filename$1"
