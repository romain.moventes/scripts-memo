# Scripts Memo

install with:

```bash
curl https://gitlab.com/romain.moventes/scripts-memo/-/raw/main/install.sh | bash
```

then add the cronjob :

```bash
crontab -e
```

add the following line for a daily backup :
```
@daily /root/backup_db_and_upload -YOUR_SERVER_NAME >> /var/log/backup_db_and_upload 2>&1
```
